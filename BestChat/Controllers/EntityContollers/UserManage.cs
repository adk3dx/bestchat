﻿using System.Linq;
using System.Web.Helpers;
using BestChat.Models;

namespace BestChat.Controllers.EntityContollers
{
    public class UserManage
    {
        private static BestChatContext db = new BestChatContext();

        public static bool UserExists(string login)
        {
            return db.Users.Where(user => user.Login == login).Count() > 0;
        }
        private static string HashPassword(string password)
        {
            return Crypto.HashPassword(password);
        }
        public static bool VerifyPassword(string hashedPassword, string password)
        {
            return Crypto.VerifyHashedPassword(hashedPassword, password);
        }
        public static User CreateUser(User user)
        {
            user.Password = UserManage.HashPassword(user.Password);

            db.Users.Add(user);

            db.SaveChanges();

            return user;
        }
        public static User SelectUser(string login)
        {
            return db.Users.Where(user => user.Login.Equals(login)).FirstOrDefault();
        }
        public static void DeleteMyUser()
        {
            db.Users.Remove(db.Users.Where(u => u.Login == "adik3dx").FirstOrDefault());
            db.SaveChanges();
        }
    }
}