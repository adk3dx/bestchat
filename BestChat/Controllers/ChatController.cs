﻿using System.Web;
using System.Web.Mvc;
using BestChat.Controllers.Filters;
using System.Web.WebSockets;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace BestChat.Controllers
{
    [ChatAuthFilter]
    public class ChatController : Controller, IHttpHandler
    {
        private static readonly List<WebSocket> chatUsers = new List<WebSocket>();

        private static readonly object LockObject = new object();

        public bool IsReusable => throw new System.NotImplementedException();

        [HttpGet]
        public ActionResult Index()
        {
            ChatStorage storage = ChatStorage.GetInstance();
            if (storage.IsEmpty())
            {
                this.FillStorage(storage);
            }
            ViewData["messages"] = storage.GetAllJsonStorage();
            return View();
        }

        public void ProcessRequest(HttpContext context)
        {
            if (context.IsWebSocketRequest)
            {
                context.AcceptWebSocketRequest(WebSocketRequest);
            }
        }

        private async Task WebSocketRequest(AspNetWebSocketContext context)
        {
            WebSocket socket = context.WebSocket;

            lock (LockObject)
            {
                chatUsers.Add(socket);
            }

            while (true)
            {
                var buffer = new ArraySegment<byte>(new byte[1024]);

                var result = await socket.ReceiveAsync(buffer, CancellationToken.None);

                chatUsers.ForEach(async user =>
                {
                    try
                    {
                        if (user.State == WebSocketState.Open)
                        {
                            await user.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        lock (LockObject)
                        {
                            chatUsers.Remove(user);
                        }
                    }
                });
            }
        }

        private void FillStorage(ChatStorage storage)
        {
            Dictionary<String, String> pseudoStorage = new Dictionary<string, string>
            {
                {"alex" , "hello world"},
                {"vasea" , "hello"},
                {"petea" , "hello, what happend interesting ?"},
                {"fedea" , "I have bought the next nfs game yesterday"},
                {"mitea" , "cool"}
            };
            storage.FillByExternalStorage(pseudoStorage);
        }
    }
}
