﻿using System.Web.Mvc;

namespace BestChat.Controllers.Filters
{
    public class ChatAuthFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if(filterContext.HttpContext.Session["userId"] == null)
            {
                filterContext.Result = new RedirectResult("/Auth/Index");
            }
        }
    }
}