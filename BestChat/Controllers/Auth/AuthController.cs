﻿using System;
using System.Web.Mvc;
using BestChat.Controllers.EntityContollers;
using BestChat.Models;

namespace BestChat.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            ViewBag.Title = "Registration";
            return View();
        }
        [HttpPost]
        public ActionResult Register()
        {
            string login = Request.Params["login"];

            if (String.IsNullOrEmpty(login) || UserManage.UserExists(login))
            {
                ViewBag.Message = "Something went wrong or user with the same login exists, try another.";
                return View("Registration");
            }

            User user = new User() {
                Login = login,
                NickName = Request.Params["nickName"],
                Password = Request.Params["password"]
            };

            user = UserManage.CreateUser(user);
            if (user.Id > 0)
            {
                this.SetSession(user);
                return Redirect("/Chat/Index");
            }

            ViewBag.Message = "something went wrong, try again later";
            return View("Registration");
        }
        [HttpPost]
        public ActionResult LogIn()
        {
            string login = Request.Params["login"];

            if (String.IsNullOrEmpty(login) || ! UserManage.UserExists(login))
            {
                ViewBag.Message = "User with this login doesn't exists.";
                return View("Index");
            }

            User user = UserManage.SelectUser(login);

            if (UserManage.VerifyPassword(user.Password, Request.Params["password"]))
            {
                this.SetSession(user);
                return Redirect("/Chat/Index");
            }

            ViewBag.Message = "something went wrong, try again later";
            return View("Index");
        }
        private void SetSession(User user)
        {
            Session["userId"] = user.Id;
            Session.Timeout = 1440;
        }
    }
}