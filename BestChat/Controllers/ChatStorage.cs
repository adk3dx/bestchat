﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BestChat.Controllers
{
    public class ChatStorage
    {
        private static ChatStorage chatStorage = null;
        private List<ChatMessage> messages = null;
        private static object lockObject = new object();

        private ChatStorage()
        {
            messages = new List<ChatMessage>();
        }

        public static ChatStorage GetInstance()
        {
            lock (lockObject)
            {
                if (chatStorage == null)
                {
                    chatStorage = new ChatStorage();
                }

                return chatStorage;
            }
        }

        public void AddMessage(string nickName, string message)
        {
            lock (lockObject)
            {
                if (messages.Count >= 100)
                {
                    messages.RemoveAt(messages.Count - 1);
                }
                messages.Add(new ChatMessage(nickName, message));
            }
        }
        public bool IsEmpty()
        {
            lock (lockObject)
            {
                return messages.Count == 0;
            }
        }

        public string GetAllJsonStorage()
        {
            lock (lockObject) {
                return JArray.FromObject(messages).ToString();
            }
        }

        public void FillByExternalStorage(Dictionary<String, String> externalStorage)
        {
            lock (lockObject)
            {
                foreach (KeyValuePair<string, string> entry in externalStorage)
                {
                    this.AddMessage(entry.Key, entry.Value);
                }
            }
        }
    }
}