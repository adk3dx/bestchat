﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BestChat.Controllers
{
    public class ChatMessage
    {
        public string nickName = null;

        public string message = null;

        public ChatMessage(string nickName, string message)
        {
            this.nickName = nickName;
            this.message = message;
        }
    }
}