﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BestChat
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "signIn",
                url: "{controller}/{action}/",
                defaults: new { controller = "Auth", action = "Index"}
            );
            routes.MapRoute(
                name: "registration",
                url: "{controller}/{action}/",
                defaults: new { controller = "Auth", action = "Registration"}
            );
            routes.MapRoute(
                name: "register",
                url: "{controller}/{action}/",
                defaults: new { controller = "Auth", action = "Register"}
            );
            routes.MapRoute(
                name: "LogIn",
                url: "{controller}/{action}/",
                defaults: new { controller = "Auth", action = "LogIn"}
            );
            routes.MapRoute(
                name: "Chat",
                url: "{controller}/{action}/",
                defaults: new { controller = "Chat", action = "Index"}
            );
        }
    }
}
