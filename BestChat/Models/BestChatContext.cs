﻿using System.Data.Entity;


namespace BestChat.Models
{
    public class BestChatContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public BestChatContext() : base(nameOrConnectionString : "BestChatContext")
        {
        }
    }
}